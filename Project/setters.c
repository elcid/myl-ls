/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   setters.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jbernabe <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/11 16:34:15 by jbernabe          #+#    #+#             */
/*   Updated: 2016/08/18 18:32:06 by jbernabe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

void		set_data(t_files **files, char *file_name, char *buff, t_flags *fl)
{
	t_info	info;

	if (file_name)
	{
		info.path = file_name;
		get_data(&info, file_name, buff);
		ft_add_node(files, info, fl);
	}
}

void		init_flags(t_flags **flags, int ac)
{
	*flags = (t_flags *)ft_memalloc(sizeof(t_flags));
	(*flags)->op_l = 0;
	(*flags)->op_br = 0;
	(*flags)->op_a = 0;
	(*flags)->op_r = 0;
	(*flags)->op_t = 0;
	(*flags)->op_1 = 0;
	(*flags)->ac = ac;
	(*flags)->counter = 0;
}

void		set_basic_list(t_files **tree, char *path, char *buff, t_flags *fl)
{
	struct dirent	*dit;
	t_info			data;
	DIR				*dir;
	char			*name;
	static int		i;

	i = 0;
	if (!path)
		path = ft_strdup(".");
	if ((dir = opendir(path)) == NULL)
	{
		perror("ls");
		return ;
	}
	while ((dit = readdir(dir)) != NULL)
	{
		name = dit->d_name;
		data.path = ft_strjoin(ft_strjoin(path, "/"), name);
		get_data(&data, name, buff);
		data.list_size = i;
		ft_add_node(tree, data, fl);
		i++;
	}
	ft_my_close(dir);
	free(dit);
}
