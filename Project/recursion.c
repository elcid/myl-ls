/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   recursion.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jbernabe <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/11 16:37:34 by jbernabe          #+#    #+#             */
/*   Updated: 2016/08/22 10:41:02 by jbernabe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

void			list_content_dir(t_flags *flags, char *buff, char *pth, int b)
{
	char		*path;
	t_files		*tree;

	tree = NULL;
	path = NULL;
	set_basic_list(&tree, pth, buff, flags);
	if (!tree)
		return ;
	if (b > 1)
	{
		ft_putstr(pth);
		ft_putendl(":");
		flags->head = pth;
	}
	print_bzise(&tree, flags);
	flags->head = pth;
	ft_memdel((void **)&pth);
	if (tree)
		display_basic_flags(&tree, flags, buff);
	if (flags->op_br == 1)
		op_recursion(&tree, flags, buff);
}

void			recursive_path(t_files *tree, t_flags *flags)
{
	t_files		*root;
	t_info		*info;
	char		buff[5];

	root = NULL;
	info = &tree->info;
	ft_bzero(buff, ft_strlen(buff));
	if (!tree)
		return ;
	ft_putstr("\n");
	ft_putstr(tree->info.path);
	ft_putendl(":");
	if (tree)
		r_content(&root, flags, buff, tree->info.path);
	op_recursion(&root, flags, buff);
}

void			op_recursion(t_files **tree, t_flags *flags, char *buff)
{
	t_files		*tmp;
	t_info		*info;

	tmp = *tree;
	info = &tmp->info;
	if (!tmp)
		return ;
	op_recursion(&tmp->left, flags, buff);
	if (S_ISDIR(info->sb.st_mode) && ROOT)
	{
		info->name = DUPATH(flags->head, info->name);
		if (flags->op_a == 1 && ft_str_hidden_path(info->name) == 1)
		{
			recursive_path(tmp, flags);
		}
		else if (ft_str_hidden_path(info->name) == 0)
		{
			recursive_path(tmp, flags);
		}
	}
	free(info->name);
	free(info->path);
	free(info);
	op_recursion(&tmp->right, flags, buff);
}
