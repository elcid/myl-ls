/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   libft.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jbernabe <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/11 16:31:58 by jbernabe          #+#    #+#             */
/*   Updated: 2016/08/11 16:32:01 by jbernabe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LIBFT_H
# define LIBFT_H

# include <sys/types.h>
# include <dirent.h>
# include <stdlib.h>
# include <sys/ioctl.h>

typedef struct		s_list
{
	void			*content;
	int				size;
	int				type_file;
	struct s_list	*next;
}					t_list;

typedef struct		s_tree
{
	void			*data;
	struct s_tree	*left;
	struct s_tree	*right;
}					t_tree;

/*
** Print standard output
*/

void				ft_putstr(const char *str);
void				ft_putstr_index(char *s, int begin, int end);
void				ft_putstr_to_char(const char *s, const char c);
void				ft_putstr_left(char *s, const int max_size);
void				ft_putnbr_endl(int number);
void				ft_print_join(char const *s1, char const *s2);
void				ft_putstr_color(const char *color, const char *s);
void				ft_putstr_into(char *space, char *s, char *end_space);
void				ft_putendl(const char *str);
void				ft_print_space(int n);
void				ft_putchar(int c);
/*
** Terminal functions
*/

struct winsize		term_sizes(void);
/*
** Str comparation from buff[n] functions
*/

void				set_max_uid_len(char *buff, char *name);
void				set_max_guid_len(char *buff, char *name);
void				set_max_nlnk(char *buff, int n_lnk);
/*
** Number comparation from buff[n]
*/

void				set_max_nbr_len(char *buff, int size_file);
void				ft_putnbr_right(const int number, const int max_size);
void				ft_putnbr_right_aux(const int number, const int max_size);
void				ft_putnbr(int n);
/*
** Deallocation memory functions
*/

void				ft_del_tab(char **t);
/*
** file treatement functions
*/

int					ft_str_hidden_path(char *s);
/*
** Binary tree functions
*/

int					ft_btree_size(t_tree *node);
void				ft_inorder(t_tree *n);
void				ft_postorder(t_tree *n);
void				ft_preorder(t_tree *n);
/*
** Allocation functions
*/

char				*ft_strdup(const char *s1);
char				*ft_strcpy(char *s1, const char *s2);
char				*ft_strcat(char *s1, const char *s2);
char				*ft_strjoin(char const *s1, char const *s2);
char				**ft_strnew_grid(int wigth, int height);
char				*ft_strnew(size_t size);
char				***ft_strnew_map(int height, int wight);
void				*ft_memalloc(size_t size);
void				*ft_realloc(void *ptr, size_t size);
/*
** deallocation memory functions
*/

void				ft_memdel(void **ap);
/*
** convert type functions
*/

char				*ft_itoa(int n);
int					ft_atoi(const char *str);
/*
** Init memory
*/

void				ft_bzero(void *s, size_t n);
/*
** size functions
*/

size_t				ft_strlen(const char *s);
int					ft_array_len(char **array);
/*
** Copy functions
*/

char				**ft_n_arraycpy(char **src, int from, int to);
void				*ft_memccpy(void *s1, const void *s2, int c, size_t n);
void				*ft_memcpy(void *s1, const void *s2, size_t n);
/*
** Compare functions
*/

int					ft_strcmp(const char *s1, const char *s2);
/*
** System functions
*/

void				ft_my_close(DIR *directory);
/*
** Sort functions
*/

void				ft_merge_sort(t_list **head);
void				ft_from_back_split(t_list *src, t_list **front_r, \
						t_list **back_r);
t_list				*ft_sorted_merge(t_list *a, t_list *b);
/*
** Search functions
*/

int					str_search_cl(const char *s1, int index, char c);
char				*ft_strrchr(const char *s, int c);
char				*ft_strstr(const char *s1, const char *s2);
char				*ft_strnstr(const char *s1, const char *s2, size_t n);
char				*ft_strchr(const char *s, int c);
/*
** Mathematical functions
*/

int					ft_abs(int i);
unsigned int		ft_nbrlen(int nbr);

#endif
