/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   data.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jbernabe <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/11 16:31:37 by jbernabe          #+#    #+#             */
/*   Updated: 2016/08/11 16:31:38 by jbernabe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <string.h>
#include "ft_ls.h"

int			check_type_file(char *file)
{
	struct stat	stat_buf;

	if (lstat(file, &stat_buf) == -1)
		return (0);
	else
	{
		if (S_ISDIR(stat_buf.st_mode) || S_ISREG(stat_buf.st_mode)
				|| S_ISLNK(stat_buf.st_mode))
			return (-1);
	}
	return (1);
}

void		set_flags(t_flags *flags, char *options)
{
	int		len;

	len = ft_strlen(options);
	if (options && options[0] == '-')
	{
		flags->op_l = str_search_cl(options, len, 'l');
		flags->op_br = str_search_cl(options, len, 'R');
		flags->op_a = str_search_cl(options, len, 'a');
		flags->op_t = str_search_cl(options, len, 't');
		flags->op_r = str_search_cl(options, len, 'r');
		flags->op_1 = str_search_cl(options, len, '1');
	}
}

t_flags		get_flags(char *options, int ac, char **av)
{
	t_flags		flags;
	int			i;

	i = 1;
	while (av[i] != NULL)
	{
		if (av[i][0] != '-')
			break ;
		options = ft_strjoin(options, &av[i][0]);
		i++;
	}
	set_flags(&flags, options);
	flags.op_1 = str_search_cl(options, ft_strlen(options), '1');
	if (flags.op_1 == 1)
		flags.op_l = 0;
	flags.ac = ac;
	free(options);
	return (flags);
}

void		get_data(t_info *file, char *name_file, char *buff)
{
	struct passwd	*owner;
	struct group	*group;

	if (lstat(file->path, &file->sb) == 1)
		perror(strerror(errno));
	if (file->sb.st_mode)
	{
		file->name = ft_strdup(name_file);
		file->error = "ok";
		file->link_count = file->sb.st_nlink;
		owner = getpwuid(file->sb.st_uid);
		group = getgrgid(file->sb.st_gid);
		file->size = file->sb.st_blocks;
		file->total_size = file->sb.st_size;
		set_max_uid_len(buff, owner->pw_name);
		set_max_guid_len(buff, group->gr_name);
		set_max_nbr_len(buff, (int)file->sb.st_size);
		set_max_nlnk(buff, file->sb.st_nlink);
	}
}
