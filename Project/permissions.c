/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   permissions.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jbernabe <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/11 16:34:07 by jbernabe          #+#    #+#             */
/*   Updated: 2016/08/22 15:33:43 by jbernabe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <time.h>
#include "ft_ls.h"

char		get_type_file(mode_t st_mode)
{
	if (S_ISBLK(st_mode))
		return ('b');
	else if (S_ISCHR(st_mode))
		return ('c');
	else if (S_ISLNK(st_mode))
		return ('l');
	else if (S_ISDIR(st_mode))
		return ('d');
	else if (S_ISFIFO(st_mode))
		return ('p');
	else if (S_ISREG(st_mode))
		return ('-');
	else if (S_ISSOCK(st_mode))
		return ('s');
	return ('-');
}

char		*get_mode_format(mode_t st_mode)
{
	char	*mode;

	mode = (char *)ft_memalloc(sizeof(char) * 10);
	mode[0] = get_type_file(st_mode);
	mode[1] = MODE(st_mode, S_IRUSR) ? 'r' : '-';
	mode[2] = MODE(st_mode, S_IWUSR) ? 'w' : '-';
	mode[3] = MODE(st_mode, S_IXUSR) ? 'x' : '-';
	mode[4] = MODE(st_mode, S_IRGRP) ? 'r' : '-';
	mode[5] = MODE(st_mode, S_IWGRP) ? 'w' : '-';
	mode[6] = MODE(st_mode, S_IXGRP) ? 'x' : '-';
	mode[7] = MODE(st_mode, S_IROTH) ? 'r' : '-';
	mode[8] = MODE(st_mode, S_IWOTH) ? 'w' : '-';
	mode[9] = MODE(st_mode, S_IXOTH) ? 'x' : '-';
	mode[3] = (MODE(st_mode, S_ISUID) && mode[3] == '-') ? 'S' : mode[3];
	mode[3] = (MODE(st_mode, S_ISUID) && mode[3] == 'x') ? 's' : mode[3];
	mode[6] = (MODE(st_mode, S_ISGID) && mode[6] == '-') ? 'S' : mode[6];
	mode[6] = (MODE(st_mode, S_ISGID) && mode[6] == 'x') ? 's' : mode[6];
	mode[9] = (MODE(st_mode, S_ISVTX) && mode[9] == '-') ? 'T' : mode[9];
	mode[9] = (MODE(st_mode, S_ISVTX) && mode[9] == 'x') ? 't' : mode[9];
	mode[10] = '\0';
	return (mode);
}

void		print_posix(t_flags *flags, t_info *file, char *buff)
{
	char			*date;
	char			*permissions;
	struct passwd	*owner;
	struct group	*group;

	owner = NULL;
	group = NULL;
	date = NULL;
	date = ctime(&file->sb.st_mtime);
	permissions = get_mode_format(file->sb.st_mode);
	if (flags->op_l)
	{
		lstat(file->name, &file->sb);
		ft_putstr(permissions);
		ft_putnbr_right((int)file->sb.st_nlink, (const int)buff[3]);
		ft_putstr(" ");
		owner = getpwuid(file->sb.st_uid);
		group = getgrgid(file->sb.st_gid);
		ft_putstr_left(owner->pw_name, (const int)buff[0]);
		ft_putstr_left(group->gr_name, (const int)buff[1]);
		ft_putnbr_right_aux((const int)file->total_size, (const int)buff[2]);
		ft_putstr_index(date, 4, ft_strlen(date) - 13);
		ft_putstr(" ");
	}
	ft_memdel((void **)&permissions);
}
