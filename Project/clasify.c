/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   clasify.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jbernabe <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/22 11:03:43 by jbernabe          #+#    #+#             */
/*   Updated: 2016/08/22 11:03:52 by jbernabe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

void	p_ocases(t_files *tree, t_flags *flags, char *buff)
{
	if (ft_strcmp(tree->info.error, "ok") == 0)
	{
		print_posix(flags, &tree->info, buff);
		ft_print_link(tree, flags);
	}
	else
		ls_error(tree);
}

void	ft_get_lnk(t_files **files, t_files **dir, t_files *lnk, t_flags *fl)
{
	struct stat	sb;
	int			err;

	read_link(&lnk);
	if (lnk->info.path && S_ISLNK(lnk->info.sb.st_mode))
	{
		err = lstat(lnk->info.path, &sb);
		if (err == -1)
		{
			if (ENOENT == errno)
			{
				lnk->info.error = ft_strdup(strerror(errno));
				ft_add_node(files, lnk->info, fl);
			}
			else
				lnk->info.error = ft_strdup(strerror(errno));
		}
		else
		{
			ft_add_node(dir, lnk->info, fl);
		}
	}
}

void inorder(t_files *r)
{
	if (!r)
		return ;
	inorder(r->left);
	ft_putendl(r->info.name);
	inorder(r->right);
}

void	sort_by_type(t_files **dir, t_files **files, t_files *node, t_flags *fl)
{
	DIR			*tmp;

	if (SORT_BY_TYPE_STATEMENT && (node->info.sb.st_mode & S_IFMT) != S_IFLNK)
		ft_add_node(files, node->info, fl);
	else if (!S_ISLNK(node->info.sb.st_mode))
	{
		if (fl->op_l == 1)
		{
			lstat(node->info.name, &node->info.sb);
			read_link(&node);
			if (errno && S_ISLNK(node->info.sb.st_mode))
			{
				ft_add_node(files, node->info, fl);
			}
			else
			{
				node->info.name = ft_strdup(node->info.name);
				ft_add_node(dir, node->info, fl);
			}
		}
		else
			ft_add_node(dir, node->info, fl);
	}
	free(tmp);
}

void	display_dir(t_files **files, t_flags *flags, char *buff, int bl)
{
	t_files		*tmp;

	tmp = *files;
	if (!*files || !tmp)
		return ;
	display_dir(&tmp->left, flags, buff, bl);
	if (!ft_strcmp(tmp->info.error, "ok"))
		p_ocases(tmp, flags, buff);
	else
	{
		list_content_dir(flags, buff, tmp->info.name, bl);
	}
	if (tmp->left || tmp->right)
		ft_putstr("\n");
	display_dir(&tmp->right, flags, buff, bl);
	free(tmp->info.path);
}
