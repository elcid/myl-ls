/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parser.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jbernabe <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/22 15:27:49 by jbernabe          #+#    #+#             */
/*   Updated: 2016/08/22 15:27:53 by jbernabe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

char		**search_index(char **src)
{
	int		i;

	i = 0;
	while (src[i])
	{
		if (ft_strcmp(src[i], "--") == 0)
		{
			src++;
			break ;
		}
		i++;
	}
	return (&*src);
}

char		**ft_flags_purge(char **src)
{
	int			i;
	int			j;
	char		**temp;
	struct stat	stat_buf;

	i = 0;
	j = 0;
	temp = (char **)malloc(sizeof(char *) * ft_array_len(src) + 1);
	while (src[i] != NULL)
	{
		if (src[i] && lstat(src[i], &stat_buf) != -1)
		{
			temp[j] = (char *)ft_memalloc(sizeof(char) * ft_strlen(src[i]));
			ft_strcpy(temp[j], src[i]);
			j++;
		}
		i++;
	}
	temp[i] = NULL;
	temp[j] = NULL;
	return (temp);
}

int			get_data_files(char **av, t_files **files, char *buff, t_flags *fl)
{
	char	**aux;
	int		i;
	char	**temp;

	aux = NULL;
	temp = NULL;
	aux = search_index(ft_flags_purge(av + 1));
	if (ft_array_len(aux) > 0 && av[1])
	{
		i = 0;
		while (aux[i] != NULL)
		{
			if (aux[i] && ft_strcmp(aux[i], av[0]) != 0)
				set_data(files, aux[i], buff, fl);
			i++;
		}
		free(aux);
		return (1);
	}
	ft_del_tab(aux);
	ft_kill_errors(av + 1);
	return (0);
}
