/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   errors.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jbernabe <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/24 19:47:01 by jbernabe          #+#    #+#             */
/*   Updated: 2016/07/24 19:47:03 by jbernabe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <sys/stat.h>
#include <sys/types.h>
#include <string.h>
#include "ft_ls.h"
#include "libft.h"

int			check_flag_error(int ac, char **files, t_flags *flags)
{
	int			i;
	struct stat	stat_buf;

	i = 1;
	while (files[i] != '\0')
	{
		if (files[i][0] != '-')
		{
			if (lstat(files[i], &stat_buf) == -1)
			{
				ft_putstr("ls: ");
				ft_putstr(ft_strjoin(files[i], ": "));
				ft_putendl(strerror(errno));
			}
		}
		else
		{
			*flags = get_flags(files[i], ac, files);
		}
		i++;
	}
	return (0);
}

void		ls_error(t_files *e)
{
	ft_putstr(e->info.name);
	ft_putendl(":");
	ft_putstr("ls: ");
	ft_putendl(e->info.error);
}

void		ft_kill_errors(char **files)
{
	int			i;
	int			kill;
	struct stat	sb;

	i = 0;
	kill = 0;
	while (i < ft_array_len(files))
	{
		if (files[i][0] != '-')
			kill = lstat(files[i], &sb);
		i++;
	}
	if (kill == -1)
		exit(0);
}
