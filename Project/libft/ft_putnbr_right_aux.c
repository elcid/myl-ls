/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putnbr_right_aux.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jbernabe <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/12 06:05:41 by jbernabe          #+#    #+#             */
/*   Updated: 2016/08/12 06:05:43 by jbernabe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void		ft_putnbr_right_aux(const int number, const int max_size)
{
	int len;
	int space;

	len = ft_nbrlen(number) + 1;
	space = max_size - len;
	if (len == max_size)
		ft_putchar(' ');
	ft_print_space((space) ? space : 0);
	ft_putnbr(number);
	ft_putchar(' ');
	return ;
}
