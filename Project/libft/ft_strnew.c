/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnew.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jbernabe <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/22 14:28:13 by jbernabe          #+#    #+#             */
/*   Updated: 2016/08/22 14:28:15 by jbernabe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char		*ft_strnew(size_t size)
{
	char	*ptr;
	size_t	i;

	ptr = 0;
	i = 0;
	ptr = (char *)ft_memalloc(sizeof(char) * (size + 1));
	if (!ptr)
		return (NULL);
	else
	{
		while (i < size + 1)
		{
			ptr[i] = '\0';
			i++;
		}
	}
	return (ptr);
}
