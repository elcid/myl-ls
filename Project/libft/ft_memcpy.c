/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memcpy.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jbernabe <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/22 11:10:33 by jbernabe          #+#    #+#             */
/*   Updated: 2016/08/22 11:10:40 by jbernabe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void			*ft_memcpy(void *s1, const void *s2, size_t n)
{
	char		*ps1;
	char const	*ps2;

	ps1 = (char *)s1;
	ps2 = (char const *)s2;
	while (n)
	{
		n--;
		*ps1++ = *ps2++;
	}
	return (s1);
}
