/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_n_arraycpy.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jbernabe <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/22 16:46:41 by jbernabe          #+#    #+#             */
/*   Updated: 2016/08/22 16:46:48 by jbernabe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	**ft_n_arraycpy(char **src, int from, int to)
{
	char	**array;
	int		i;

	i = 0;
	array = (char **)ft_memalloc(sizeof(char *) * to);
	while (from < to)
	{
		if (src[from])
		{
			ft_putendl(src[from]);
			array[i] = ft_strdup(src[from]);
		}
		i++;
		from++;
	}
	array[i] = NULL;
	return (array);
}
