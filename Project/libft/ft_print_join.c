/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_join.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jbernabe <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/17 18:22:55 by jbernabe          #+#    #+#             */
/*   Updated: 2016/08/22 11:48:47 by jbernabe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_print_join(char const *s1, char const *s2)
{
	char *str;

	if (s1 && s2)
	{
		str = ft_strjoin(s1, s2);
		ft_putstr(str);
	}
	return ;
}
