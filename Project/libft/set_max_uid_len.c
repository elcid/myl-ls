/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   set_max_uid_len.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jbernabe <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/11 16:32:57 by jbernabe          #+#    #+#             */
/*   Updated: 2016/08/22 12:57:07 by jbernabe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
**	compare buff[0]
**	if new len is  longer  set the new value
**	set the max value between of buff[0]
*/

void		set_max_uid_len(char *buff, char *name)
{
	int		size;

	size = ft_strlen(name);
	if (size > buff[0])
	{
		buff[0] = size;
	}
}
