/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_my_close.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jbernabe <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/22 02:36:53 by jbernabe          #+#    #+#             */
/*   Updated: 2016/08/22 11:17:23 by jbernabe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void			ft_my_close(DIR *directory)
{
	if (directory && closedir(directory))
	{
		exit(EXIT_FAILURE);
	}
}
