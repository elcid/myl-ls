/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_str_hidden_path.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jbernabe <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/11 19:35:31 by jbernabe          #+#    #+#             */
/*   Updated: 2016/08/22 12:15:33 by jbernabe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static int	ft_check_end(char *s, int *b)
{
	int		j;

	j = ft_strlen(s);
	while (j >= 0)
	{
		if (s[j] == '/')
			break ;
		j--;
	}
	j++;
	if (s[j] == '.')
		*b = 1;
	else
		*b = 0;
	return (*b);
}

int			ft_str_hidden_path(char *s)
{
	int		b;

	b = 0;
	return (ft_check_end(s, &b));
}
