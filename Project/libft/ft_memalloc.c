/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memalloc.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jbernabe <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/22 11:04:58 by jbernabe          #+#    #+#             */
/*   Updated: 2016/08/22 11:05:10 by jbernabe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memalloc(size_t size)
{
	void	*ptr;

	ptr = 0;
	if (size == 0)
		return (NULL);
	else
	{
		ptr = malloc(size);
		if (!ptr)
			return (NULL);
		else
		{
			ft_bzero(ptr, size);
			return (ptr);
		}
	}
}
