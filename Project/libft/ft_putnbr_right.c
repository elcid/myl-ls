/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putnbr_right.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jbernabe <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/11 18:28:22 by jbernabe          #+#    #+#             */
/*   Updated: 2016/08/11 18:28:25 by jbernabe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void		ft_putnbr_right(const int number, const int max_size)
{
	int		len;

	len = ft_nbrlen(number) - 1;
	ft_print_space(max_size - len);
	ft_putnbr(number);
}
