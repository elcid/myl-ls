/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_inorder.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jbernabe <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/22 11:01:35 by jbernabe          #+#    #+#             */
/*   Updated: 2016/08/22 11:01:44 by jbernabe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void		ft_inorder(t_tree *n)
{
	if (n != NULL)
	{
		ft_inorder(n->left);
		ft_putendl((char *)n->data);
		ft_inorder(n->right);
	}
}
