/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putstr_color.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jbernabe <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/20 12:05:52 by jbernabe          #+#    #+#             */
/*   Updated: 2016/06/20 12:17:44 by jbernabe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void		ft_putstr_color(const char *color, const char *s)
{
	if (!color)
		ft_putstr("\033[31m");
	else
		ft_putstr(color);
	ft_putstr(s);
	ft_putendl("\033[m");
}
