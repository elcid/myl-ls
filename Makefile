# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: jbernabe <marvin@42.fr>                    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2016/07/24 18:47:37 by jbernabe          #+#    #+#              #
#    Updated: 2016/08/11 19:27:14 by jbernabe         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

my_ls:
	make -C  ../moulitest/ft_ls_tests
	@make link

simple:
	 @make -C Project
	 @rm  -rf ft_ls
	 @make link

test:
	$(MAKE) -C Project
	../moulitest/ft_ls_tests/ft_ls_test

link:
	rm -rf ft_ls
	ln -s Project/ft_ls .

clean:
	make clean -C Project

fclean:
	make fclean -C Project
	@rm -rf ft_ls

re: fclean  my_ls

.PHONY: re, clean, fclean test
