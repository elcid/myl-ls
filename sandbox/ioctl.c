#include <termios.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
# include <errno.h>
#include <sys/ioctl.h>

struct termios saved_attributes;

void 
reset_input_mode (void)
{
  tcsetattr (STDIN_FILENO, TCSANOW, &saved_attributes);
}

int	main(void)
{
	struct termios  config;
	   speed_t speed;

	if(!isatty(STDIN_FILENO))
	{
		perror(strerror(errno));
		exit(EXIT_FAILURE);
	}
	if(tcgetattr(STDIN_FILENO, &saved_attributes) < 0)
	{
		perror(strerror(errno));
		exit(EXIT_FAILURE);
	}
	config.c_iflag = 0;

	// config.c_oflag &= ~(OCRNL | ONLCR | ONLRET|
					 // ONOCR | ONOEOT| OFILL | OPOST);
	config.c_oflag = 0;

	config.c_lflag &= ~(ICANON|ECHO);;
	// config.c_lflag = 0;
	config.c_cflag = 0;
	tcgetattr (STDIN_FILENO, &config);
	fcntl(STDIN_FILENO, F_SETFL, O_NONBLOCK);

	config.c_cc[VMIN]  = 1;
	config.c_cc[VTIME] = 0;
	 tcsetattr(STDOUT_FILENO,TCSAFLUSH,&config);
	 atexit (reset_input_mode);
	 char c;

  while (1)
    {
      read (STDIN_FILENO, &c, 1);
      if (c == '\004')          /* C-d */
        break;
      else
        putchar (c);
    }

    return EXIT_SUCCESS;
}
