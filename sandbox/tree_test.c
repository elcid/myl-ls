#include <stdio.h>
#include <stdlib.h>

typedef struct s_info	t_info;
typedef struct s_files	t_files;

struct s_info
{
	char				*name;
};

struct s_files
{
	t_info				info;
	char				*path;
	int					type_file;
	int					index;
	struct s_files		*right;
	struct s_files		*left;
};

char		*ft_strcpy(char *s1, const char *s2)
{
	char	*p_s1;

	p_s1 = s1;
	while (*s2)
	{
		*p_s1 = *s2;
		++p_s1;
		++s2;
	}
	*p_s1 = '\0';
	return (s1);
}

int			ft_strcmp(const char *s1, const char *s2)
{
	while (*s1 == *s2)
	{
		if (*s1 == '\0')
			return (0);
		s1++;
		s2++;
	}
	if (*(unsigned char *)s1 > *(unsigned char *)s2)
		return (1);
	return (-1);
}

int			ft_strlen(char *s)
{
	int	i;

	i = 0;
	while(s[i])
		i++;
	return (i);
}

char		*ft_strdup(char *s)
{
	char	*tmp;
	int		i;

	i = 0;
	tmp = (char *)malloc(sizeof(char) * ft_strlen(s) + 1);
	if (!tmp)
		return (NULL);
	ft_strcpy(tmp, s);
	return (tmp);
}

t_files			*ft_new_node(t_info data)
{
	t_files		*node;
	static int	i;

	node = (t_files*)malloc(sizeof(t_files));
	if (!node)
	{
		return NULL;
	}
	node->info = data;
	node->type_file = 0;
	node->index = i;
	node->path = ft_strdup(data.name);
	node->left = NULL;
	node->right = NULL;
	i++;
	return (node);
}

void		ft_insert(t_files **tree, t_info info)
{
	if (*tree == NULL)
	{
		*tree = ft_new_node(info);
	}
	else
	{
		if (ft_strcmp((*tree)->info.name, info.name) > 0)
		{
			ft_insert(&((*tree))->left, info);
		}
		else
		{
			ft_insert(&((*tree))->right, info);
		}
	}
}

t_files		*ft_min_value(t_files *node)
{
	t_files		*tmp;

	tmp = node;
	while (tmp->left != NULL)
	{
		tmp = tmp->left;
	}
	return (tmp);
}

static void		ft_replace(t_files **tree, t_files **aux)
{
	if ((*tree)->right != NULL)
	{
		ft_replace(&(*tree)->right, aux);
	}
	else
	{
		(*aux)->info.name = ft_strdup((*tree)->info.name);
		*aux = *tree;
		*tree = (*tree)->left;
	}
}

void	ft_delete_tnode(t_files **tree, char *s)
{
	t_files	*tmp;

	tmp = NULL;
	if (*tree == NULL)
		return ;
	if (ft_strcmp((*tree)->info.name, s) < 0)
	{
		ft_delete_tnode(&(*tree)->right, s);
	}
	else if (ft_strcmp((*tree)->info.name, s) > 0)
	{
		ft_delete_tnode(&(*tree)->left, s);
	}
	else if (ft_strcmp((*tree)->info.name, s) == 0)
	{
		tmp = *tree;
		if ((*tree)->left == NULL)
		{
		printf("test3 \n");
			*tree = (*tree)->right;
		}
		else if ((*tree)->right == NULL)
		{
		printf("test \n");
			*tree = (*tree)->left;
		}
		else
		{
		printf("test2 \n");
			ft_replace(&(*tree)->left, &tmp);
		}
		free(tmp);
	}
}

void		ft_inorden(t_files *tree)
{
	if (!tree)
		return ;
	ft_inorden(tree->right);
	printf("name node [%s]\n", tree->info.name);
	ft_inorden(tree->left);
}

void		ft_inorden_delete(t_files **tree)
{
	t_files	*tmp;

	tmp = *tree;
	if (!(*tree))
		return ;
	ft_inorden_delete(&(tmp)->right);
	if (ft_strcmp((tmp)->info.name, "tutu") == 0)
	{
		printf("here is [%s]\n", (tmp)->info.name);
		ft_delete_tnode(tree, "tutu");
	}
	ft_inorden_delete(&(tmp)->left);
}

int			main(void)
{
	t_files *root;
	t_info	d1;
	t_info	d2;
	t_info	d3;
	t_info	d4;

	root = NULL;
	d1.name = "toto";
	d2.name = "tata";
	d3.name = "tutu";
	d4.name = "titi";
	ft_insert(&root, d1);
	ft_insert(&root, d2);
	ft_insert(&root, d3);
	ft_insert(&root, d4);
	ft_inorden(root);
	printf("\n delete \n");
	ft_inorden_delete(&root);
	// ft_delete_tnode(&root, "tata");
	printf("\n after delete \n");
	ft_inorden(root);
	return (0);
}
